﻿using System;
using System.Globalization;

namespace SearchByPatternInText
{
#pragma warning disable
    public static class Searcher
    {
        public static int[] SearchPatternString(this string text, string pattern, bool overlap)
        {
            if (pattern == null)
            {
                throw new ArgumentException(null, nameof(pattern));
            }

            if (text == null)
            {
                throw new ArgumentException(null, nameof(text));
            }

            int[] buffer = new int[text.Length];
            int count = 0;

            pattern = pattern.ToLower();
            text = text.ToLower();

            for (int i = 0, k = 0; i < text.Length - pattern.Length + 1; i++)
            {
                bool flag = false;

                if (pattern[0] == text[i])
                {
                    for (int j = 0; j < pattern.Length; j++)
                    {
                        if (text[i + j] == pattern[j])
                        {
                            flag = true;
                        }
                        else if (overlap)
                        {
                            flag = false;
                            break;
                        }
                        else if (!overlap)
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                
                if (flag)
                {
                    buffer[k] = i + 1;
                    count++;
                    k++;
                    if (!overlap)
                    {
                        i += pattern.Length;
                    }
                }
            }

            int[] result = new int[count];

            for (int i = 0; i < count; i++)
            {
                result[i] = buffer[i];
            }

            return result;
        }
    }
}
